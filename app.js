const express = require('express');
const fs = require('fs');
const multer = require('multer');
const bodyParser = require('body-parser');
const path = require('path')

const app = express();

const storage = multer.diskStorage({
    destination: "public/uploads/",
    filename: function (req, file, cb) {
        cb(
            null,
            file.fieldname + "-" + Date.now() + path.extname(file.originalname)
        );
    }
});

const upload = multer({
    storage: storage
}).single('fileupload');

app.use(express.static('./public'));
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
    
    let html = `
<!DOCTYPE html>
    <html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Beginner</title>
    <link rel="stylesheet" href="index.css">

</head>
    <div>
    <div class= "header"> 
    <h1>Welcome to KenzieGram</h1>
    </div>
    <form action="/uploads" method="post" enctype="multipart/form-data">
    <input type="file" name="fileupload" value="fileupload" id="fileupload>
    <label for="fileupload">Select a file to upload</label>
    <button type="upload" class="btn">Submit</>
    </div> 
    </form>`

    const paths = './public/uploads';
    fs.readdir(paths, function (err, items) {
        console.log(items);
        for (i = 0; i< items.length; i++) {
         html += `<img src="/uploads/${items[i]}">`
        }
        
        res.send(html)
    })
});

app.post('/uploads', upload, (req, res) => {

    
    res.send(
        `
    <!DOCTYPE html>
    <html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Beginner</title>
    <link rel="stylesheet" href="index.css">
</head>
        <body>
        <div class= "backButton"> 
        <a href="/">Back</a>
        </div>
        <img src="/uploads/${req.file.filename}">
        </body>
        <div>
        </div>
        `);
});

const port = 3000;
app.listen(port, () => console.log(`Server started on ${port}`));

